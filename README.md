# RuoYi-Jdao-Cloud 剑道云快速开发平台

<div style="height: 10px; clear: both;"></div>

- - -
## 平台简介（v1.0.1）

[![RuoYi-Jdao-Cloud](https://img.shields.io/badge/RuoYi-v3.8.7-success.svg)](https://gitee.com/ljlleo/RuoYi-Jdao-Cloud)
[![RuoYi-Jdao-Cloud](https://img.shields.io/badge/RuoYi_Jdao_Cloud-1.0.0-success.svg)](https://gitee.com/ljlleo/RuoYi-Jdao-Cloud)
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-2.7.18-blue.svg)]()
[![JDK-1.8](https://img.shields.io/badge/JDK-1.8-green.svg)]()

- 本仓库为前端技术栈（[Vue2](https://cn.vuejs.org) + [Element](https://github.com/ElemeFE/element) + [Vue CLI](https://cli.vuejs.org/zh)），请移步[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue/tree/master/ruoyi-ui)。
- 项目代码、文档 均开源免费可商用 遵循开源协议在项目中保留开源协议文件即可<br>
活到老写到老 为兴趣而开源 为学习而开源 为让大家真正可以学到技术而开源

### 友情链接

- [若依/RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue) 版本
 
## 前端运行

```bash
# 克隆项目
git clone https://github.com/ljlleo/RuoYi-Jdao-Cloud-Vue2.git

# 安装依赖
yarn install --registry=https://registry.npmmirror.com

# 启动服务
yarn dev

```

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 系统体验

- 浏览器访问 http://localhost:81
- admin/admin123

## 剑道云交流群

QQ群：  [![加入QQ群](https://img.shields.io/badge/605109586-blue.svg)](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=lqMHu_5Fskm7H2S1vNAQTtzAUokVydwc&authKey=ptw0Fpch5pbNocML3CIJKKqZBaq2DI7cusKuzIgfMNiY3t9Pvd9hP%2BA8WYx3yaY1&noverify=0&group_code=605109586)
