import request from '@/utils/request'

// 查询模板管理列表
export function listTemplate(query) {
  return request({
    url: '/tool/template/page',
    method: 'get',
    params: query
  })
}

// 查询模板管理详细
export function getTemplate(id) {
  return request({
    url: '/tool/template/' + id,
    method: 'get'
  })
}

// 新增模板管理
export function addTemplate(data) {
  return request({
    url: '/tool/template',
    method: 'post',
    data: data
  })
}

// 修改模板管理
export function updateTemplate(data) {
  return request({
    url: '/tool/template',
    method: 'put',
    data: data
  })
}

// 删除模板管理
export function delTemplate(id) {
  return request({
    url: '/tool/template/' + id,
    method: 'delete'
  })
}

// 复制模板管理
export function copyTemplate(data) {
  return request({
    url: '/tool/template/copy',
    method: 'post',
    data: data
  })
}

