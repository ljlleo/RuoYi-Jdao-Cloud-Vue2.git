import request from '@/utils/request'

// 查询模板分组列表
export function pageTemplateGroup(query) {
  return request({
    url: '/tool/templateGroup/page',
    method: 'get',
    params: query
  })
}

// 查询模板分组列表
export function listTemplateGroup(query) {
  return request({
    url: '/tool/templateGroup/list',
    method: 'get',
    params: query
  })
}

// 查询模板分组详细
export function getTemplateGroup(id) {
  return request({
    url: '/tool/templateGroup/' + id,
    method: 'get'
  })
}

// 新增模板分组
export function addTemplateGroup(data) {
  return request({
    url: '/tool/templateGroup',
    method: 'post',
    data: data
  })
}

// 修改模板分组
export function updateTemplateGroup(data) {
  return request({
    url: '/tool/templateGroup',
    method: 'put',
    data: data
  })
}

// 删除模板分组
export function delTemplateGroup(id) {
  return request({
    url: '/tool/templateGroup/' + id,
    method: 'delete'
  })
}
